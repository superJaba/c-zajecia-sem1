﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.kurs = new System.Windows.Forms.TextBox();
            this.euro = new System.Windows.Forms.Button();
            this.pln = new System.Windows.Forms.Button();
            this.kwotaEU = new System.Windows.Forms.TextBox();
            this.kwotaPL = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(227, 114);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Kurs EURO";
            // 
            // kurs
            // 
            this.kurs.Location = new System.Drawing.Point(308, 111);
            this.kurs.Name = "kurs";
            this.kurs.Size = new System.Drawing.Size(100, 20);
            this.kurs.TabIndex = 1;
            // 
            // euro
            // 
            this.euro.Location = new System.Drawing.Point(308, 151);
            this.euro.Name = "euro";
            this.euro.Size = new System.Drawing.Size(100, 23);
            this.euro.TabIndex = 2;
            this.euro.Text = "EURO ->";
            this.euro.UseVisualStyleBackColor = true;
            this.euro.Click += new System.EventHandler(this.button1_Click);
            // 
            // pln
            // 
            this.pln.Location = new System.Drawing.Point(308, 196);
            this.pln.Name = "pln";
            this.pln.Size = new System.Drawing.Size(100, 23);
            this.pln.TabIndex = 3;
            this.pln.Text = "<- PLN";
            this.pln.UseVisualStyleBackColor = true;
            this.pln.Click += new System.EventHandler(this.pln_Click);
            // 
            // kwotaEU
            // 
            this.kwotaEU.Location = new System.Drawing.Point(432, 174);
            this.kwotaEU.Name = "kwotaEU";
            this.kwotaEU.Size = new System.Drawing.Size(100, 20);
            this.kwotaEU.TabIndex = 4;
            // 
            // kwotaPL
            // 
            this.kwotaPL.Location = new System.Drawing.Point(187, 174);
            this.kwotaPL.Name = "kwotaPL";
            this.kwotaPL.Size = new System.Drawing.Size(100, 20);
            this.kwotaPL.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.kwotaPL);
            this.Controls.Add(this.kwotaEU);
            this.Controls.Add(this.pln);
            this.Controls.Add(this.euro);
            this.Controls.Add(this.kurs);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private System.Windows.Forms.TextBox kwotaEU;
        private System.Windows.Forms.TextBox kwotaPL;

        private System.Windows.Forms.Button euro;

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox kurs;
        private System.Windows.Forms.Button pln;

        #endregion
    }
}