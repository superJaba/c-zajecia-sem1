﻿using System;

namespace _04_12_2021
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] tablica = new int[100];
            Random random = new Random();
            for (int i = 0; i < tablica.Length; i++)
            {
                tablica[i] = random.Next(0, 7);
            }

            statystyka(tablica);

            int[,] nowaTablica = new int[20, 20];
            for (int i = 0; i < 20; i++)
            {
                for (int j = 0; j < 20; j++)
                {
                    nowaTablica[i, j] = random.Next(1, 1000);
                    Console.Write($" {nowaTablica[i,j]}");
                }
                Console.WriteLine();
            }

            MaxKolumnySumaWiersza(nowaTablica);
            Zadanie3();
            ZadanieSamodzielne();
        }

        private static void ZadanieSamodzielne()
        {
            /*
             * Napisać funkcję symulującą grę w statki przeciwko komputerowi. Zakładamy, że tylko człowiek „strzela”.
Na początku komputer ma wygenerować tablicę 10x10 losowo wypełnioną maksymalnie 10 jednomasztowcami
(nie musisz przejmować się, jeśli zdarzy się, że wylosowane będzie już zajęte miejsce). Tablica ta jest
nieznana użytkownikowi. W kolejnym kroku program pyta użytkownika o koordynaty strzału (numer wiersza
i kolumny). Po oddanym strzale następuje wypisanie komunikatu: „Pudło!”, „Już strzelałeś/łaś w to pole!”,
„Trafiony, zatopiony!”. Gra trwa dopóki użytkownik nie strąci wszystkich statków. Po zakończonej grze należy wypisać 
komunikat, ile strzałów zużył użytkownik.
             */
            
            
        }

        private static void Zadanie3()
        {
            //Konwerter temp
            // char z = char.Parse(Console.ReadLine());
            // char c = char.Parse(Console.ReadLine());
            double[] tablica =  {-1, 15, 23.5, 0, -273.16};
            int wynik = zamiana(tablica, 'c', 'k');
            Console.WriteLine($"Wynik konwersji: {wynik}");

        }

        private static int zamiana(double[] tablica, char z, char c)
        {
            int wynik = 1;
            for (int i = 0; i < tablica.Length; i++)
            {
                if (z == 'c' && c == 'f')
                {
                    tablica[i] = (tablica[i] * 1.8) + 32;
                }

                if (z == 'c' && c == 'k')
                {
                    tablica[i] += 273.15;
                    if (tablica[i] <= 0)
                    {
                        return 0;
                    }
                }

                if (z == 'k' && c == 'f') tablica[i] = (tablica[i] * 1.8) - 459.67;
                if (z == 'k' && c == 'c')
                {
                    tablica[i] -= 273.15;
                    if (tablica[i] <= 0) return 0;
                }

                if (z == 'f' && c == 'c') tablica[i] = (tablica[i] - 32) / 1.8;
                if (z == 'f' && c == 'k')
                {
                    tablica[i] = (tablica[i] + 459.67) * 5 / 9;
                    if (tablica[i] <= 0) return 0;
                }
            }
            return wynik;
        }

        private static void MaxKolumnySumaWiersza(int[,] nowaTablica)
        {
            
            
        }


        private static void statystyka(int[] tab)
        {
            int[] licz = new int[7];
            foreach (int element in tab)
            {
                licz[element]++;
            }

            for (var i = 0; i <= 6; i++)
            {
                if (licz[i] > 30)
                {
                    Console.WriteLine($"Za dużo wyników {i}");
                }
                else
                {
                    Console.Write($"{i}");
                    for (int j = 0; j < licz[i]; j++)
                    {
                        Console.Write("*");
                    }

                    Console.WriteLine();
                }
            }
        }
    }
}