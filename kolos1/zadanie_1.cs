﻿using System;

namespace kolos1
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             * Zadanie 1

            Program prosi użytkownika o podanie wieku dwóch osób i wskaz, która z nich jest starsza. 
            Jeśli obie osoby maja powyżej 100 lat, program powinien zachować się w szczególny sposób.
             */
            
            Console.Write("Podaj wiek pierwszej osoby: ");
            bool wiek1flag = int.TryParse(Console.ReadLine(), out int wiek1);
            Console.Write("Podaj wiek drugiej osoby: ");
            bool wiek2flag = int.TryParse(Console.ReadLine(), out int wiek2);

            if (wiek1flag && wiek2flag)
            {
                if (wiek1 > wiek2) Console.WriteLine("Pierwsza osoba jest starsza.");
                else Console.WriteLine("Druga osoba jest starsza.");
                if (wiek1 > 100 && wiek2 > 100) Console.WriteLine("Gratulacje dla 100 latków !!");
            }
            else
            {
                Console.WriteLine("Podano niepoprawne dane.");
            }
        }
    }
}
