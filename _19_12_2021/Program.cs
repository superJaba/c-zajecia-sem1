﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using static System.Environment;

namespace _19_12_2021
{
    class Program
    {
        static void Main(string[] args)
        {
            OsAndDirectoryInfo.GetSystemData();
            //Zad1();
            // Zad2();
            //Zad3();
            // Zad5();
            //Zad6();
            Zad7();
            // ZadanieSamodzielne();
        }

        private static void ZadanieSamodzielne()
        {
            string FilePathCheck()
            {
                string s = "";
                s = Console.ReadLine();
                if (s != null && !s.ToLower().Contains(@"c:\"))
                {
                    s = @"C:\" + s;
                }
                return s;
            }
            
            /*
             * 8. Zadania samodzielne
            • Wczytać nazwę pliku, wyświetlić ile razy w pliku występuje każda litera
            • Wczytać nazwę pliku, wyświetlić ilość liczb parzystych (sprawdzić czy plik i katalog istnieje)
             */

            Console.Write("Podaj nazwe pliku do zliczenia liter: ");
            var filePath = FilePathCheck();

            Dictionary<char, int> zliczWystepowanie = new Dictionary<char, int>();

            if (File.Exists(filePath))
            {
                StreamReader reader = new StreamReader(new FileStream(filePath, FileMode.Open, FileAccess.Read));
                while (!reader.EndOfStream)
                {
                    var fileRow = "";
                    try
                    {
                        fileRow = reader.ReadLine();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                   
                    var charArray = fileRow.ToCharArray();
                    foreach (var c in charArray)
                    {
                        if (!zliczWystepowanie.ContainsKey(c))
                        {
                            zliczWystepowanie.Add(c, 1);
                        }
                        else
                        {
                            zliczWystepowanie[c] += 1;
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("Podany plik nie istnieje.");
            }
            foreach( KeyValuePair<char, int> kvp in zliczWystepowanie )
            {
                Console.WriteLine("char = {0}, Value = {1}", kvp.Key, kvp.Value);
            }
            
            // Zadanie samodzilene b)
            //• Wczytać nazwę pliku, wyświetlić ilość liczb parzystych (sprawdzić czy plik i katalog istnieje)
            
            Console.Write("Podaj nazwe pliku do zliczenia ilosci liczb parzystych: ");
            var filePathValidated = FilePathCheck();
            int counter = 0;
            int rt = filePathValidated.LastIndexOf(@"\", StringComparison.Ordinal);
            if (File.Exists(filePathValidated) && Directory.Exists(filePathValidated.Substring(0, rt)))
            {
                StreamReader streamReader = new StreamReader(new FileStream(filePathValidated, FileMode.Open, FileAccess.Read));
                while (!streamReader.EndOfStream)
                {
                    var readLine = streamReader.ReadLine();
                    bool readLineFlag = int.TryParse(readLine, out int parsedInt);
                    if (readLineFlag)
                    {
                        if (parsedInt % 2 == 0)
                        {
                            counter++;
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("Plik albo folder nie istnieje");
                return;
            }

            Console.WriteLine($"Liczb parzystych w pliku było: {counter}");

        }

        private static void Zad7()
        {
            /*
         * 7. Tworzenie - lista folderów 
             W pliku tekstowym lista.txt lista folderów do utworzenia, proszę napisać program, który je utworzy.
            Plik załączony do tematu na moodle
            Nazwy folderów należy wczytać z pliku
            Foldery będą tworzyć się jako podfoldery folderu PLIKI na C:\
         */
            var sysName = OsAndDirectoryInfo.ReturnSystemId().ToLower();
            string resourceListaTxt;
            if (sysName.Contains("win")) resourceListaTxt = @"..\..\..\resource\lista.txt";
            else resourceListaTxt = @"~/RiderProjects/podstawy programowania C#/_19_12_2021/resource/";
            StreamReader streamReader;
            try
            {
                streamReader =
                    new StreamReader(new FileStream(resourceListaTxt, FileMode.Open, FileAccess.Read));
            }
            catch (FileNotFoundException e)
            {
                throw new FileNotFoundException("Nie znaleziono pliku do wczytania." + e);
            }

            while (!streamReader.EndOfStream)
            {
                var fileRow = streamReader.ReadLine();
                if (fileRow != null && sysName.Contains("win") && !fileRow.ToLower().Contains(@"c:\"))
                {
                    fileRow = @"C:\" + fileRow;
                }
                else if (fileRow != null && !fileRow.ToLower().Contains(@"~/"))
                {
                    fileRow = @"~/" + fileRow;
                }

                if (Directory.Exists(fileRow)) Console.WriteLine($"Podany katalog juz istnieje: {fileRow}");
                else
                {
                    UtworzKatalog(fileRow);
                }
            }
        }

        private static void UtworzKatalog(string catalogPath)
        {
            Console.WriteLine($"Tworzenie nowego katalogu: {catalogPath}");
            try
            {
                Directory.CreateDirectory(catalogPath);
            }
            catch (Exception e)
            {
                throw new Exception($"Wystąpił błąd podczas tworzenia katalogu:\n {e.Message}");
            }
        }

        private static void Zad6()
        {
            /*
             * 6. Tworzenie folderu:
            • Zapytać o nazwę folderu i utworzyć go na C:\
            • Program ma sprawdzać i tworzyć folder w \
            Polecenie tworzące folder* to:
                Directory.CreateDirectory(nazwa);
            *lista większej ilości dostępnych poleceń w treści wykładu
            Sprawdzanie czy folder NIE istnieje na dysku C:
                if (!a.Contains(@"C:\"))
            W zmiennej a wprowadzam nazwę, w nawiasie – lokalizację, gdzie chcę sprawdzić czy istnieje. 
             */

            //sprawdzenie systemu operacyjnego

            Console.Write("Podaj nazwe katalogu do sprawdzenia: ");

            var linuxFilePath = @"/home/user/RiderProjects/podstawy programowania C#/_19_12_2021/";
            string a = Console.ReadLine();
            a.ToLower();
            //dla Linuxa
            var os = OSVersion;
            if (!os.VersionString.Contains("Microsoft Windows"))
            {
                var directoryFlag = Directory.Exists(linuxFilePath + a);
                if (directoryFlag)
                {
                    Console.WriteLine("Podany katalog juz istnieje -" + linuxFilePath + a);
                }
                else
                {
                    Console.WriteLine("Tworzenie folderu w folderze appki - " + a);
                    Directory.CreateDirectory(linuxFilePath + a);
                    Console.WriteLine("utworzono folder: " + a);
                }
            }
            else
            {
                //dla windowsa
                if (!a.ToLower().Contains(@"c:\"))
                {
                    a = @"C:\" + a;
                }

                var directoryFlag = Directory.Exists(a);
                if (directoryFlag)
                {
                    Console.WriteLine($"Podany katalog już istnieje: {a}");
                }
                else
                {
                    Console.WriteLine(@"Tworzenie folderu w katalogu domowym");
                    Directory.CreateDirectory(@a);
                    Console.WriteLine("utworzono folder: " + a);
                }
            }
        }

        private static void Zad5()
        {
            //Dla pliku uzyskanego w pop. zadaniu wyświetlić wartości powyżej średniej

            var filePath = @"C:\pliki\zad2.bin";
            var linuxFilePath = @"/home/user/RiderProjects/podstawy programowania C#/_19_12_2021/pliki/zad2.bin";
            BinaryReader reader = new BinaryReader(new FileStream(linuxFilePath, FileMode.Open, FileAccess.Read));
            int[] tablica = new int[1000];
            int suma = new int();
            for (int i = 0; i < tablica.Length; i++)
            {
                tablica[i] = reader.ReadInt32();
                suma += tablica[i];
            }

            reader.Close();

            double average = (double) suma / 1000;
            Console.WriteLine("Średnia wynosi {0} a suma {1}", average, suma);
            foreach (int i in tablica)
            {
                if (i > average) Console.Write(" " + i);
            }
        }

        private static void Zad3()
        {
            var filePath = @"C:\pliki\zad2.bin";
            BinaryReader binaryReader =
                new BinaryReader(new FileStream(filePath, FileMode.Open, FileAccess.Read));
            int[] tablica = new int[1000];
            for (int i = 0; i < tablica.Length; i++)
            {
                tablica[i] = binaryReader.ReadInt32();
                Console.Write("{0} ", tablica[i]);
            }

            binaryReader.Close();

            int szukana = int.Parse(Console.ReadLine());
            bool jest = false;
            foreach (int i in tablica)
            {
                if (szukana == i) jest = true;
            }

            if (jest) Console.WriteLine("JEST");
            else Console.WriteLine("BRAK");
        }

        private static void Zad2()
        {
            /*
             * File.Exists("lokalizacja\nazwa_pliku") -> true - istnieje, false - brak plika
             * File.Delete("lokalizacja\nazwa_pliku") -> kasowanie plika
             * Ciąg rosnący:
             * Tablica[i] = tablica[i-1]+random;
             *
             * Proszę wylosować tablicę 1000 liczb losowych rosnących i zapisac w pliku BIN
             * Jeśli plik istnieje, usunąć plik przed zapisem
             */

            int[] tablica = new int[1000];
            Random random = new Random();
            tablica[0] = random.Next(1, 10);
            for (int i = 1; i < tablica.Length; i++)
            {
                tablica[i] = tablica[i - 1] + random.Next(1, 10);
            }

            var filePath = @"C:\pliki\zad2.bin";
            var linuxFilePath = @"/home/user/RiderProjects/podstawy programowania C#/_19_12_2021/pliki/zad2.bin";
            if (File.Exists(linuxFilePath))
            {
                File.Delete(linuxFilePath);
                Console.WriteLine("plik {0} został skasowany", filePath);
            }

            BinaryWriter writer =
                new BinaryWriter(new FileStream(linuxFilePath, FileMode.Create, FileAccess.Write));
            foreach (int element in tablica)
            {
                writer.Write(element);
            }

            writer.Close();
        }

        private static void ReadBinaryFile(string filePath)
        {
            if (!File.Exists(filePath))
            {
                Console.WriteLine("Nie ma takiego plika");
                return;
            }
        }

        private static void Zad1()
        {
            int[] t = new int[1000];
            Random random = new Random();
            for (int i = 0; i < t.Length; i++)
            {
                t[i] = random.Next(1, 1000);
            }

            StreamWriter writer =
                new StreamWriter(new FileStream(@"C:\pliki\1912.txt", FileMode.OpenOrCreate, FileAccess.Write));
            BinaryWriter binaryWriter =
                new BinaryWriter(new FileStream(@"C:\pliki\1912.dat", FileMode.OpenOrCreate, FileAccess.Write));
            foreach (var item in t)
            {
                writer.WriteLine(item);
                binaryWriter.Write((int) item);
            }

            writer.Close();
            binaryWriter.Close();

            //wczytaj wasrtosci z plików i zsumować

            StreamReader reader =
                new StreamReader(new FileStream(@"C:\pliki\1912.txt", FileMode.Open, FileAccess.Read));
            BinaryReader BR =
                new BinaryReader(new FileStream(@"C:\pliki\1912.dat", FileMode.Open, FileAccess.Read));

            int sum = new int();
            while (!reader.EndOfStream)
            {
                sum += int.Parse(reader.ReadLine() ?? throw new InvalidOperationException());
            }

            int sumB = 0;
            for (int i = 0; i < 1000; i++)
            {
                sumB += BR.ReadInt32();
            }

            Console.Write("Suma z txt: {0}, Suma z bi: {1}", sum, sumB);
        }
    }
}