﻿using System;
using static System.Environment;

namespace _19_12_2021
{
    public static class OsAndDirectoryInfo
    {
       public static void GetSystemData()
        {
            var os = OSVersion;
            Console.WriteLine("\nCurrent OS Information:");
            Console.WriteLine("Platform: {0:G}", os.Platform);
            Console.WriteLine("Version String: {0}", os.VersionString);
            Console.WriteLine();
        }
       
       public static string ReturnSystemId()
       {
           var os = OSVersion;
           PlatformID platformId = os.Platform;
           return platformId.ToString();
       }
       
    }
}