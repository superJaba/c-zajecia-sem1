﻿using System;
using System.Collections;

namespace kolos1_zad3
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             * Napisać program, który wczytuje od użytkownika liczby, do momentu aż zostanie podana liczba 0.
            Po zakończeniu działania programu wyświetlona zostaje informacja o sumie oraz średniej z wszystkich 
            podanych liczb  oraz jak wiele podanych zostało liczb ujemnych, nieparzystych.
            Przykład:
            4 2 3 6 1 6 1 0
            Wynik:
            7 1 0
             */

            IList list = new ArrayList();
            int userInput = 0;
            do
            {
                Console.Write("Podaj liczbę: ");
                bool inputFlag = int.TryParse(Console.ReadLine(), out userInput);
                if (inputFlag)
                {
                    list.Add(userInput);
                }
                
            } while (userInput != 0);

            SumAll(list);
            Srednia(list);
            IleUjemnychIleNieparzystych(list);

        }

        private static void IleUjemnychIleNieparzystych(IList list)
        {
            int ujemne = 0, nieparzyste = 0;
            foreach (var ele in list)
            {
                if ((int) ele < 0) ujemne++;
                if ((int) ele % 2 != 0) nieparzyste++;
            }

            Console.WriteLine($"Ujemnych liczb podano {ujemne}, nieparzystych było: {nieparzyste}");
        }

        private static void Srednia(IList list)
        {
            int  suma = 0;
            foreach (var element in list)
            {
                suma += (int) element;
            }

            Console.WriteLine($"Średnia wynosi: {(double)suma/(list.Count - 1)}");
        }

        private static void SumAll(IList list)
        {
            int suma = 0;
            foreach (var element in list)
            {
                Console.Write(element + " ");
                suma += (int) element;
            }
            Console.WriteLine();
            Console.WriteLine($"Suma wynosi: {suma}");
        }
    }
}