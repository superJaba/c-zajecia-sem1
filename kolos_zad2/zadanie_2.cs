﻿using System;

namespace kolos_zad2
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             * Zadanie 2

Program losuje do tablicy 100 liczb z zakresu <1,100000>, następnie  wyświetlający na ekranie kolejne liczby, 
które są podzielne bez reszty przez n (gdzie n jest zadawane z klawiatury). Program podaje ile taki liczb było.
             */

            int[] tablica = new int[100];
            Random random = new Random();

            for (int i = 0; i < tablica.Length; i++)
            {
                tablica[i] = random.Next(1, 100000);
            }
            
            Console.Write("Podaj liczbę przez którą chcesz dzielić: ");
            bool flag = int.TryParse(Console.ReadLine(), out int dzielnik);
            int counter = 0;

            if (flag && dzielnik > 0)
            {
                foreach (int liczba in tablica)
                {
                    if (liczba % dzielnik == 0)
                    {
                        Console.WriteLine(liczba);
                        counter++;
                    }
                }   
            }
            else throw new Exception("Podano niepoprawną liczbę lub mniejszą lub równą 0.");

            Console.WriteLine($"Było {counter} w tablicy podzielnych przez {dzielnik}");
        }
    }
}