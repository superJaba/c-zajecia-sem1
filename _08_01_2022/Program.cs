﻿using System;

namespace _08_01_2022
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            Zadanie1();
        }

        private static void Zadanie1()
        {
            KodZLaborkow zLaborkow = new KodZLaborkow();
            // zLaborkow.Cw1();
            // zLaborkow.Cw2();
            // zLaborkow.Cw3();
            // zLaborkow.Cw4(false, true);
            // zLaborkow.Cw5();
            // zLaborkow.Cw6();
            ZadaniaPDF zadaniaPdf = new ZadaniaPDF();
            // zadaniaPdf.Zad4PDF();
            zadaniaPdf.Zad5PDF();
        }
    }
}