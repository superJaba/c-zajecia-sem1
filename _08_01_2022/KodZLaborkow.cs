﻿using System;
using System.IO;
using System.Text;

namespace _08_01_2022
{
    public class KodZLaborkow
    {
        public KodZLaborkow()
        {
        }

        // zamienia char na wartosc ASCI
        public void Cw1()
        {
            Console.Write("Podaj jakiś tam łańcuch znakowy: ");
            string a = Console.ReadLine();
            int dlugosc = a.Length;
            for (int i = 0; i < dlugosc; i++)
            {
                Console.WriteLine(a[i] - '0');
            }
        }

        // dodaje do siebie skladowe cyfry z liczby
        public void Cw2()
        {
            int DodajCyfry(string a)
            {
                int s = 0;
                for (int i = 0; i < a.Length; i++)
                {
                    s += a[i] - 48;
                }

                return s;
            }

            Console.Write("Podaj liczby oddzielone spacja, zakończ enterem: ");
            string inputData;
            inputData = Console.ReadLine();
            string[] splitArray = inputData.Split(" ");
            foreach (var element in splitArray)
            {
                Console.WriteLine("suma cyfr {0} wynosi {1}", element, DodajCyfry(element));
            }
        }

        // czy wyrażenie jest anagramem -> otto - toto
        public void Cw3()
        {
            int[] asciCodes = new int[256];
            Console.Write("Podaj pierwszy wyraz: ");
            string word1 = Console.ReadLine();
            Console.Write("Podaj drugi wyraz: ");
            string word2 = Console.ReadLine();
            if (word1.Length == word2.Length)
            {
                for (int i = 0; i < word1.Length; i++)
                {
                    asciCodes[(int) word1[i]]++;
                }

                for (int i = 0; i < word2.Length; i++)
                {
                    asciCodes[(int) word2[i]]--;
                }

                bool c = true;
                foreach (var item in asciCodes)
                {
                    if (item != 0) c = false;
                }

                if (c) Console.WriteLine("To są anagramy");
                else Console.WriteLine("To nie są anagramy");
            }
            else
            {
                Console.WriteLine("Napisy są innej długości.");
            }
        }

        // szyfrowanie metodą płotu -> zabieramy jakies litery z podanego wyrazu i z obu tworzymy nowy wyraz
        public void Cw4(bool zadanieMoje, bool zWczytanymPrzesunieciem)
        {
            //moje
            if (zadanieMoje)
            {
                Console.Write("Podaj wyraz do zapłotowania: ");
                string input = Console.ReadLine();
                StringBuilder word1 = new StringBuilder();
                var charArray = input.ToCharArray();
                for (int i = 0; i < charArray.Length; i++)
                {
                }
            }
            //wykładowcy stałe przesunieceie
            else
            {
                if (zWczytanymPrzesunieciem)
                {
                    Console.Write("Podaj wyraz do zapłotowania: ");
                    string a = Console.ReadLine();
                    string w = "";
                    Console.Write("Podaj przesunięcie: ");
                    int p = int.Parse(Console.ReadLine());
                    for (int j = 0; j < p; j++)
                    {
                        for (int i = j; i < a.Length; i += p)
                        {
                            w = w + a[i];
                        }
                    }

                    Console.WriteLine(w);
                }
                else
                {
                    Console.Write("Podaj wyraz do zapłotowania: ");
                    string a = Console.ReadLine();
                    string w = "";
                    int p = int.Parse(Console.ReadLine());
                    for (int i = 0; i < a.Length; i += 2)
                    {
                        w = w + a[i];
                    }

                    for (int i = 1; i < a.Length; i += 2)
                    {
                        w = w + a[i];
                    }

                    Console.WriteLine(w);
                }
            }
        }


        //porównywanie stringów
        public void Cw5()
        {
            Console.Write("Podaj pierwszy wyraz: ");
            string word1 = Console.ReadLine();
            Console.Write("Podaj drugi wyraz: ");
            string word2 = Console.ReadLine();
            if (string.Compare(word1, word2, true) != 0) Console.WriteLine("Wyrażenia różne");
            else Console.WriteLine("Wyrażenia podobne");
        }

        public void Cw6()
        {
            string file1 = @"C:\pliki\tekst1.txt";
            string file2 = @"C:\pliki\tekst2.txt";
            if (File.Exists(file1) && File.Exists(file2))
            {
                StreamReader stringReader1 =
                    new StreamReader(new FileStream(file1, FileMode.Open, FileAccess.Read));
                StreamReader stringReader2 =
                    new StreamReader(new FileStream(file2, FileMode.Open, FileAccess.Read));
                bool rowne = true;
                string l1, l2;
                while (!stringReader1.EndOfStream && !stringReader2.EndOfStream)
                {
                    l1 = stringReader1.ReadLine();
                    l2 = stringReader2.ReadLine();
                    if (string.Compare(l1, l2, false) != 0)
                    {
                        rowne = false;
                        Console.WriteLine("Różnica \'{0}\' \'{1}\'", l1, l2);
                    }
                }

                if (rowne) Console.WriteLine("Pliki mają równą zawartość");
            }
            else Console.WriteLine("Brak plika");
        }
    }
}