﻿using System;
using System.IO;
using System.Text;

namespace _08_01_2022
{
    public class ZadaniaPDF
    {
        private static string _nieMaTakiegoPlika = "Nie ma takiego plika";

        public ZadaniaPDF()
        {
        }

        private string ValidateFile(string filePath, bool creationMode)
        {
            string validatedPath = "";
            if (!filePath.Contains(@"C:\"))
            {
                validatedPath = @"C:\" + filePath;
            }

            if (!File.Exists(validatedPath) && !creationMode)
            {
                validatedPath = _nieMaTakiegoPlika;
            }
            return validatedPath;
        }

        public void Zad4PDF()
        {
            /*
             * 4. Zadanie
                Wczytać linię tekstu, następnie wczytać wyraz. 
                Z wyrażenia usunąć dany wyraz w każdym jego wystąpieniu. 
                Wczytać linię tekstu, następnie wczytać wyraz.
                Metody:
                • string.Contains(napis)
                • string.Remove(OdZnaku,IleZnaków)
                • napis.IndexOf(napis)
             */
            Console.Write("Podaj lokalizację i nazwe plika do sprawdzenia: ");
            string userInputFile = ValidateFile(Console.ReadLine(), false);
            Console.Write("Podaj wyraz do usunięcia: ");
            string userInputWordToRemove = Console.ReadLine();
            if (userInputFile.Equals(_nieMaTakiegoPlika))
            {
                Console.WriteLine("Nie można przeprowadzić zadania - brak plika wejściowego");   
            }
            else
            {
                StreamReader reader = new StreamReader(new FileStream(@userInputFile, FileMode.Open, FileAccess.Read));
                while (!reader.EndOfStream)
                {
                    try
                    {
                        string line = reader.ReadLine();
                        if (line.Contains(userInputWordToRemove))
                        {
                            var indexOfUserWord = line.IndexOf(userInputWordToRemove);
                            Console.WriteLine(indexOfUserWord);
                            var remove = line.Remove(indexOfUserWord, userInputWordToRemove.Length);
                            Console.WriteLine("Line after word was removed: \'" + remove + "\'");
                        }
                    }
                    catch (NullReferenceException e)
                    {
                        Console.WriteLine(e.Message);
                        throw new NullReferenceException(_nieMaTakiegoPlika);
                    }
                }
                reader.Close();
            }

        }

        struct Person
        {
            public string firstName, lastName;
            public int age;

        }
        
        public void Zad5PDF()
        {
            Person[] persons = null;
            Console.Write("Ile osób chcesz zapisac: ");
            bool numFlag = int.TryParse(Console.ReadLine(), out int numOfPersons);
            if (numFlag && numOfPersons > 0)  persons = new Person[numOfPersons];
            else persons = new Person[1]; 

            string checkNameLastName(string varName)
            {
                string userInput;
                while (true)
                {
                    Console.Write("Podaj " + varName);
                    userInput = Console.ReadLine();
                    if (string.IsNullOrEmpty(userInput)) continue;
                    break;
                }
                return userInput;
            }

            int getAge()
            {
                int age;
                Console.Write("Podaj wiek: ");
                bool ageFlag = int.TryParse(Console.ReadLine(), out age);
                if (!ageFlag)
                {
                    Console.WriteLine("Podano niepoprawny wiek");
                    getAge();
                }
                return age;
            }

            Console.Write("Podaj ścieżkę oraz nazwę pliku csv: ");
            string inputPath = Console.ReadLine();
            string filePath = ValidateFile(inputPath, true);

            for (int i = 0; i < persons.Length; i++)
            {
                persons[i].firstName = checkNameLastName("imie: ");
                persons[i].lastName = checkNameLastName("nazwisko: ");
                persons[i].age = getAge();
            }

            StreamWriter writer = new StreamWriter(new FileStream(filePath, FileMode.Create, FileAccess.Write));
            try
            {
                writer.WriteLine("Imie;Nazwisko;Wiek");
                foreach (var person in persons)
                {
                    StringBuilder builder = new StringBuilder();
                    builder.Append(person.firstName);
                    builder.Append(';');
                    builder.Append(person.lastName);
                    builder.Append(';');
                    builder.Append(person.age);
                    builder.Append(';');
                    writer.WriteLine(builder.ToString());
                    Console.WriteLine("Zapisuje do pliku: " + builder);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw new Exception();
            }
            writer.Close();
        }
    }
}