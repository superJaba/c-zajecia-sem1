﻿using System;
using System.IO;

namespace _05_12_2021
{
    class Program
    {
        static void Main(string[] args)
        {
            // Random random = new Random();
            // int[] tablica = new int[100000];
            // for (int i = 0; i < tablica.Length; i++)
            // {
            //     tablica[i] = random.Next(1, 101);
            // }
            //
            // int[] wyniki = new int[101];
            // // for (int i = 0; i < tablica.Length; i++)
            // // {
            // //     wyniki[tablica[i]]++;
            // // }
            // foreach (int i in tablica)
            // {
            //     wyniki[i]++;
            // }
            //
            // for (int i = 0; i < wyniki.Length; i++)
            // {
            //     Console.WriteLine("Liczba {0} wystąpiła {1}", i, wyniki[i]);
            // }

            //@ powoduje ze string nie jest interpretowany !
            // FileStream P = new FileStream(@"c:\pliki\jeden.txt", FileMode.OpenOrCreate, FileAccess.Write);
            // StreamWriter streamWriter = new StreamWriter(P);
            // int a;
            // for (int i = 0; i < 10; i++)
            // {
            //     a = int.Parse(Console.ReadLine());
            //     streamWriter.WriteLine(a);
            // }
            // streamWriter.Close();

            //wylosuj 1000 liczb do pliku txt z zakresu <-100, 100>
            // FileStream stream = new FileStream(@"c:\pliki\dwa.txt", FileMode.OpenOrCreate, FileAccess.Write);
            // StreamWriter writer = new StreamWriter(stream);
            // Random random = new Random();
            // for (int i = 0; i < 1000; i++)
            // {
            //     writer.WriteLine(random.Next(-100, 100));
            // }
            // writer.Close();

            //odczytanie pliku
            // FileStream fileStream = new FileStream(@"C:\pliki\jeden.txt", FileMode.Open, FileAccess.Read);
            // StreamReader streamReader = new StreamReader(fileStream);
            // string a;
            // while (!streamReader.EndOfStream)
            // {
            //     a = streamReader.ReadLine();
            //     Console.WriteLine(a);
            // }
            // streamReader.Close();
            
            //z pliku z 1000 liczb wczytac liczby i zsumowac
            // try
            // {
            //     FileStream fileStream = new FileStream(@"c:\pliki\dwa.txt", FileMode.Open, FileAccess.Read);
            //     StreamReader streamReader = new StreamReader(fileStream);
            //     int suma = new();
            //     while (!streamReader.EndOfStream)
            //     {
            //         suma += int.Parse(streamReader.ReadLine() ?? string.Empty);
            //     }
            //     streamReader.Close();
            //
            //     Console.WriteLine($"Suma wynosi {suma}");
            // }
            // catch (FileNotFoundException e)
            // {
            //     Console.WriteLine(e.Message);
            // }
            
            //wczytuje linie tekstu i zapisuje do pliku 
            // StreamWriter streamWriter =
            //     new StreamWriter(new FileStream(@"c:\pliki\trzy.txt", FileMode.OpenOrCreate, FileAccess.Write));
            // Console.Write("Podaj zdanie do zapisania: ");
            // string s = Console.ReadLine();
            // streamWriter.Write(s);
            // streamWriter.Close();
            
            //odczytac z plikow p1 i p2 linie i zapisac do pliku wyjsciowego naprzemiennie wartosci (p1 l1, p2 l1, p1 l2, p2 l2...)
            // try
            // {
            //     StreamReader streamReader =
            //         new StreamReader(new FileStream(@"c:\pliki\p1.txt", FileMode.Open, FileAccess.Read));
            //     StreamReader streamReader2 =
            //         new StreamReader(new FileStream(@"c:\pliki\p2.txt", FileMode.Open, FileAccess.Read));
            //     FileStream fileStream = new FileStream(@"c:\pliki\pw.txt", FileMode.OpenOrCreate, FileAccess.Write);
            //     StreamWriter streamWriter = new StreamWriter(fileStream);
            //     string a;
            //     while (!streamReader.EndOfStream && !streamReader2.EndOfStream)
            //     {
            //         a = streamReader.ReadLine();
            //         streamWriter.WriteLine(a);
            //         a = streamReader2.ReadLine();
            //         streamWriter.WriteLine(a);
            //     }
            //     streamWriter.Close();
            // }
            // catch (FileNotFoundException e)
            // {
            //     Console.WriteLine(e);
            // }
            
            /*
             * 11. Zadania
                1) Program zapisuje w pliku wynikowe linie o parzystych numerach z pliku źródłowego.
                2) Wylosować 100 liczb parzystych do pliku. 
             */

            Console.WriteLine("Podaj numer zadania do przesłania: \n1 - Zapisz do pliku parzyste linie z pliku źródłowego. \n2 - Losuje i zapisuje 100 liczb parzystych. \n0 - Koniec");
            var readLine = int.TryParse(Console.ReadLine(), out int option);
            if (readLine)
            {
                switch (option)
                {
                    case 1:
                        ZapiszDoPlikuParzysteLinieZPlikuZrodlowego();
                    break;
                    case 2:
                        WylosujDoPliku();
                        break;
                    default:
                        break;
                }
            }
            else throw new FormatException("Podano nie poprawną opcje.");

            WylosujDoPliku();

        }

        private static void ZapiszDoPlikuParzysteLinieZPlikuZrodlowego()
        {
            try
            {
                StreamReader reader =
                    new StreamReader(new FileStream(@"C:\pliki\pw.txt", FileMode.Open, FileAccess.Read));
                StreamWriter writer = new StreamWriter(new FileStream(@"C:\pliki\parzyste_linie.txt",
                    FileMode.OpenOrCreate, FileAccess.Write));
                int counter = 1;
                while (!reader.EndOfStream)
                {
                    var readLine = reader.ReadLine();
                    if (counter % 2 == 0)
                    {
                        writer.WriteLine(readLine);
                        Console.WriteLine("Zapisano do pliku: {0}", readLine);
                    }
                    counter++;
                }
                reader.Close();
                writer.Close();
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private static void WylosujDoPliku()
        {
            Random random = new Random();
            int counter = 0;
            try
            {
                FileStream stream =
                    new FileStream(@"C:\pliki\parzyste.txt", FileMode.OpenOrCreate, FileAccess.ReadWrite);
                StreamWriter writer = new StreamWriter(stream);
                do
                {
                    int tempNumber = random.Next(0, 100);
                    if (tempNumber % 2 == 0)
                    {
                        writer.WriteLine(tempNumber);
                        counter++;
                    }
                } while (counter < 100);
                writer.Close();
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine(e);
                throw;
            }

        }
    }
}