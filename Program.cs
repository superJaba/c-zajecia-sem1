﻿using System;

namespace _08_01_2022
{
    class Program
    {
        static void Main(string[] args)
        {
            Zadanie1();
        }

        private static void Zadanie1()
        {
            int DodajCyfry(string a)
            {
                int s = 0;
                for (int i = 0; i < a.Length; i++)
                {
                    s += a[i] - 48;
                }

                return s;
            }

            string readLine;
            readLine = Console.ReadLine();
            string[] t = readLine.Split(" ");
            foreach (var e in t)
            {
                Console.WriteLine("suma cyfr {0} wynosi {1}", e, DodajCyfry(e));
            }
        }
    }
}