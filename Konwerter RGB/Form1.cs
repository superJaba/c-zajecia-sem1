﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Konwerter_RGB
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public string z(string a)
        {
            while (a.Length < 2)
            {
                a = "0" + a;
            }

            return a;
        }

        private void konwert_Click(object sender, EventArgs e)
        {
            int red = int.Parse(textBox1.Text);
            int green = int.Parse(textBox2.Text);
            int blue = int.Parse(textBox3.Text);
            string hexVal = z(red.ToString("X") + z(green.ToString("X") + z(blue.ToString("X"))));
            textBox4.Text = '#'+hexVal;
            
            //error
            MessageBox.Show("Niepoprawne dane", "Błędne dane", MessageBoxButtons.OK);
            
        }
    }
}