﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace menuExample
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void zakończToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void otwórzToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox1.Clear();
                string plik = openFileDialog1.FileName;
                FileStream stream = new FileStream(plik, FileMode.Open, FileAccess.Read);
                StreamReader reader = new StreamReader(stream);
                StringBuilder linia = new StringBuilder();
                while (!reader.EndOfStream)
                {
                    linia.Append(reader.ReadLine());
                    linia.AppendLine();
                }
                textBox1.Text = linia.ToString() ;
                reader.Close();
            }
        }

        private void zapiszToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog1.Title = "Zapisz plik";
            saveFileDialog1.RestoreDirectory = true;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                FileStream stream = (FileStream) saveFileDialog1.OpenFile();
                stream.
                writer.Write(textBox1.Text);
            }

        }
    }
}