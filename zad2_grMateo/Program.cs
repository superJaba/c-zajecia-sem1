﻿using System;
using System.IO;
using System.Text;

namespace zad2_grMateo
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader reader = new StreamReader(
                new FileStream(@"C:\pliki\motta.txt", FileMode.Open, FileAccess.Read));
            StringBuilder builder = new StringBuilder();

            while (!reader.EndOfStream)
            {
                builder.Append(reader.ReadLine());
            }
            Console.WriteLine(builder);

            string allText = builder.ToString();
            // string contains = allText.Replace("...", ".");
            string[] stringArray = allText.Split(".");

            Console.WriteLine();
            foreach (var s in stringArray)
            {
                Console.WriteLine(s);
            }

            Console.WriteLine(stringArray.Length);
            int counter = 0;
            for (int i = 0; i < stringArray.Length; i++)
            {
                if (stringArray[i].Length != 0 )
                {
                    
                    counter++;
                    if (stringArray[i].Contains('?'))
                    {
                        string[] newArr = stringArray[i].Split("?");
                        counter += newArr.Length;
                    }
                }
                
            }

            Console.WriteLine($"Ilosc zdan: {counter}");
        }
    }
}