﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kalkulator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double a, b;
            if (double.TryParse(textBox1.Text, out a) && double.TryParse(textBox2.Text, out b))
            {
                if (radioButton1.Checked) textBox4.Text = (a + b).ToString();
                if (radioButton2.Checked) textBox4.Text = (a - b).ToString();
                if (radioButton4.Checked) textBox4.Text = (a * b).ToString();
                if (radioButton3.Checked) textBox4.Text = (a / b).ToString();
            }
            else
            {
                MessageBox.Show("Błędne dane", "Błędne dane", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            } 
        }
        

        private void radioButton1_Click(object sender, EventArgs e)
        {
            double a, b;
            if (double.TryParse(textBox1.Text, out a) && double.TryParse(textBox2.Text, out b))
            {
                textBox4.Text = (a + b).ToString();
            }
            else
            {
                MessageBox.Show("Błędne dane", "Błędne dane", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            } 
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            double a;
            if (double.TryParse(textBox1.Text, out a)) button1.Enabled = true;
            else button1.Enabled = false;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            double b;
            if (double.TryParse(textBox2.Text, out b))
            {
                button1.Enabled = true;
                groupBox1.Enabled = true;
            }
            else
            {
                button1.Enabled = false;
                groupBox1.Enabled = false;
            }
        }
    }
}